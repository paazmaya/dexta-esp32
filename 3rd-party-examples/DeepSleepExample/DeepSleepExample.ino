
#include <time.h>
#include <sys/time.h>
#include "driver/rtc_io.h"

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  5        /* Time ESP32 will go to sleep (in seconds) */

#define TZName "EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00" /* Europe/Helsinki */

RTC_DATA_ATTR int bootCount = 0;

int LED_PIN_RED = 2;
int LED_PIN_GREEN = 14;
int LED_PIN_YELLOW = 15;

int LED_INT_FLASH = 4;
int LED_INT_RED = 33;

int CURRENT_PIN = 0;

time_t now;
char strftime_buf[64];
struct tm timeinfo;

/*
  Method to print the reason by which ESP32
  has been awaken from sleep
  From https://randomnerdtutorials.com/esp32-timer-wake-up-deep-sleep/
*/
void print_wakeup_reason() {
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch (wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 :
      Serial.println("Wakeup caused by external signal using RTC_IO");
      break;
    case ESP_SLEEP_WAKEUP_EXT1 :
      Serial.println("Wakeup caused by external signal using RTC_CNTL");
      break;
    case ESP_SLEEP_WAKEUP_TIMER :
      Serial.println("Wakeup caused by timer");
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD :
      Serial.println("Wakeup caused by touchpad");
      break;
    case ESP_SLEEP_WAKEUP_ULP :
      Serial.println("Wakeup caused by ULP program");
      break;
    default :
      Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason);
      break;
  }
}
void setup() {
  setenv("TZ", TZName, 1); /* Overwrite */
  tzset();

  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();

  pinMode(LED_PIN_RED, OUTPUT);
  pinMode(LED_PIN_GREEN, OUTPUT);
  pinMode(LED_PIN_YELLOW, OUTPUT);
  pinMode(LED_INT_FLASH, OUTPUT);
  pinMode(LED_INT_RED, OUTPUT);

  delay(500);

  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);

  Serial.print("Boot count ");
  Serial.println(bootCount);

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  if (bootCount == 0) {
  }

  switch (bootCount % 4) {
    case 0 :
      CURRENT_PIN = LED_PIN_RED;
      break;
    case 1 :
      CURRENT_PIN = LED_PIN_GREEN;
      break;
    case 2 :
      CURRENT_PIN = LED_PIN_YELLOW;
      break;
    case 3 :
      CURRENT_PIN = LED_INT_FLASH;
      break;
    case 4 :
      CURRENT_PIN = LED_INT_RED;
      break;
    default :
      CURRENT_PIN = LED_PIN_RED;
  }
  digitalWrite(CURRENT_PIN, HIGH);

  bootCount++;
  delay(5000);

  digitalWrite(CURRENT_PIN, LOW);

  /* Push button reboot alternative */
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_13, 0);

  /* Timer based reboot alternative */
  /*esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);*/

  esp_deep_sleep_start();
}

void loop() {

}
