# dexta-esp32

ESP32 based micro computer collecting data and controlloring lights of Fordson Dexta (1961) tractor.

## Providing 5VDC while tractor uses 12VDC

The voltage provided by the charger can vary between 12...14 Volts, and the electronics
that are planned here use strict 5 Volts.
The DC-DC step down converter needs to output steady 5V while its input can vary,
and should support up to 24V, which happens to be the case in many such devices.

Not only does the voltage vary, there is also ripple from the conversion, that needs
to be reduced, in order to keep microcontrollers alive.


Voltage for ESP32 and its parts, are regulated to 5V with a ...
"Mini360 3A DC Voltage Step Down Power Converter 3.3V Module Buck 5V 9V P2A3"
https://goughlui.com/2018/07/04/tested-mini-360-mp2307-based-3a-buck-converter-module/
Suggests not to use it, in the light power consumption.

"TO-220 DC-DC 5V Rectifier Converter Replace LM7805 Voltage Regulator Step-down Module"


## What is covered by microcontroller

"ESP32-CAM Development Board WIFI Bluetooth Camera Module OV2640 +Antenna"


Programming to ESP32 board happens via USB to TTL device:
"USB To RS232 TTL PL2303HX Auto Converter Module Converter Adapter"

or

Serial Converter CP2102
- Drivers https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers
- Datasheet https://cdn.sparkfun.com/datasheets/BreakoutBoards/CP2102_v1.2.pdf

### Lightning

Lights are operated via push buttons, powered via transistor (TIP127 5A 100V PNP Darlington transistor TO-220).
https://www.componentsinfo.com/tip127-transistor-pinout-equivalent/

Various lights are used:

* Work lights, four (two front, two rear), each ...
* Front lights white, six, each 12.8VDC 40mA led
* Rear lights red, four, each 12.8VDC 40mA led
* Bottom lights blue, eight, each 12.8VDC 40mA led
* Informational lights are in the main module, such as "power on" led

Lights are connected via 18AWG cable.

### Sensors

* Coolant temperature
* Engine oil pressure
* Engine rpm
* Voltage, from the generator https://startingelectronics.org/articles/arduino/measuring-voltage-with-arduino/
* How often hydraulic lever is moved pass certain points (Infrared Slotted Optical Speed Measuring Sensor Optocoupler Test Module)

### Casing for ESP32

Weather proof case, with inline connectors, that can be mounted underneath the steering housing.

Custom PCB board can be ordered via:

* https://www.pcbway.com/orderonline.aspx


### Other things to account for

"Tight Braided PET Expandable Sleeving Cable Wire Sheath, blue, 12 mm"

Numbers are displaid on:
"0.96" I2C IIC Serial 128X64 128*64 Blue OLED LCD LED Display Module"


Safe shutdown with capasitor, since a small lipo battery might be too much and connection to main battery needs to be completely cut off, in order to avoid any drain on the main battery while the tractor is not operated.

https://www.hackster.io/nickthegreek82/esp32-deep-sleep-tutorial-4398a7

## Development environment

There are few flavours...


### ESP-idf

https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html
I did this in macOS, for "The Espressif IoT Development Framework (esp-idf). ESP-IDF is the official development framework for the ESP32 chip":

```sh
brew install python3
ln -s /usr/local/bin/python3.8 /usr/local/bin/python
brew install cmake ninja dfu-util
pip3 install -U pip

mkdir -p ~/esp
cd ~/esp
git clone --recursive https://github.com/espressif/esp-idf.git

cd ~/esp/esp-idf
./install.sh # will install to ~/.espressif
```

Edit `~/.bashrc` to contain the following line:

```sh
alias get_idf='. $HOME/esp/esp-idf/export.sh'
```

### Arduino IDE

Following the guide here
https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-mac-and-linux-instructions/

Open "Preferences" and add `https://dl.espressif.com/dl/package_esp32_index.json`
to "Additional Board Manager URLs".
Then in "Tools > Board > Boards Manager" search for "ESP32 by Espressif Systems" and install it.


The first ESP32-CAM board that I have in the shelf:

```
Chip is ESP32D0WDQ6 (revision 1)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
```

### PlatformIO

`platformio.ini` is for its configuration

## Learning from others

The folder called `3rd-party-examples` contains several scripts from varying tutorials,
each linked in the given script, to try out different aspects that are needed for this project.

### Using another ESP32-CAM to see what is happening when not there

Following first
https://randomnerdtutorials.com/esp32-cam-pir-motion-detector-photo-capture/
then adding Wifi access point with a static IP (`10.0.1.1`)
https://randomnerdtutorials.com/esp32-cam-access-point-ap-web-server/
but since the access point consumes plenty of energy,
it should be only on via dip switch or a push button.

How to set the time, so that the meta data and file names can use date and time?

Parts used:

* ESP32-CAM, with camera module OV2640
* HC-SR501 Adjust IR Pyroelectric Infrared PIR Motion Sensor Detector Module
* MicroSD card formatted as `FAT32` file system, 8 GB, made by TDK

https://github.com/raphaelbs/esp32-cam-ai-thinker

Biggest resolution supported is UXGA (1600x1200, 2 megapixels).

Since the microSD card is inserted, it will utilise many of the IO ports, as well as
the camera will do so, hence the available physical pins are limited.


### Unlocking features with NFC

Breakout board HW-147 (in ebay.com it was PN532) with main chip that has the following texts:

```txt
5321
CHT358
03
TSD1905
NXP
```

Closest to this
https://www.electroschematics.com/nfc-rfid-module-pn532/

## License

MIT
